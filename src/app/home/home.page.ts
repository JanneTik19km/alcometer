import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private weight: number;
  private genders = [];
  private gender: string;
  private time: number;
  private bottles: number;
  private litres: number;
  private grams: number;
  private burning: number; 
  private left: number;
  private promilles: number;
  

constructor() { }

  ngOnInit() {

    this.genders.push('Male');
    this.genders.push('Female');

    

  }
  calculate() {

    let factor = 0;

    switch (this.gender) {
      case 'Male':
        factor = 0.7;
        break;
      case 'Female':
        factor = 0.6;
        break;

    }

    if (this.gender === 'Male') {
      this.litres = (this.bottles * 0.33);
      this.grams = (this.litres * 8 * 4.5);
      this.burning = (this.weight / 10);
      this.left = (this.grams - (this.burning * this.time));
      this.promilles = (this.left / (this.weight * factor));
    }
    if (this.gender === 'Female') {
      this.litres = (this.bottles * 0.33);
      this.grams = (this.litres * 8 * 4.5);
      this.burning = (this.weight / 10);
      this.left = (this.grams - (this.burning * this.time));
      this.promilles = (this.left / (this.weight * factor));
    
    }
      

  }
}